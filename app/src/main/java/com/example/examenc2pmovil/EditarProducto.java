package com.example.examenc2pmovil;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.examenc2pmovil.Modelo.ProductoDb;

public class EditarProducto extends AppCompatActivity {

    private Button btnBuscar,btnActualizar, btnCerrar, btnBorrar;
    private EditText txtCodigo, txtNombre, txtMarca, txtPrecio;
    private RadioButton rbtnPerecedero, rbtnNoPerecedero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_producto);
        iniciarComponentes();

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {buscarProducto();}
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {salir();}
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {actualizarProducto();}
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {eliminarProducto();}
        });

    }

    private void iniciarComponentes() {
        btnBuscar = findViewById(R.id.btnBuscar);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnCerrar = findViewById(R.id.btnCerrar);
        txtCodigo = findViewById(R.id.txtCodigo);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        rbtnPerecedero = findViewById(R.id.rbtnPerecedero);
        rbtnNoPerecedero = findViewById(R.id.rbtnNoPerecedero);
    }

    private void buscarProducto() {
        String codigo = txtCodigo.getText().toString().trim();
        if (!codigo.isEmpty()) {
            ProductoDb productoDb = new ProductoDb(EditarProducto.this);
            Producto producto = productoDb.getProductoPorCodigo(codigo);

            if (producto != null) {
                mostrarDatosProducto(producto);
            } else {
                limpiarCampos();
                mostrarToast("Producto no encontrado");
            }
        } else {
            mostrarToast("Ingrese un código válido");
        }
    }

    private void mostrarDatosProducto(Producto producto) {
        txtNombre.setText(producto.getNombre());
        txtMarca.setText(producto.getMarca());
        txtPrecio.setText(String.valueOf(producto.getPrecio()));

        rbtnPerecedero.setChecked(producto.isPerecedero());
        rbtnNoPerecedero.setChecked(!producto.isPerecedero());
    }

    private void actualizarProducto() {
        String codigo = txtCodigo.getText().toString().trim();
        String nombre = txtNombre.getText().toString().trim();
        String marca = txtMarca.getText().toString().trim();
        String precioString = txtPrecio.getText().toString().trim();

        // Validar que los campos no estén vacíos
        if (codigo.isEmpty() || nombre.isEmpty() || marca.isEmpty() || precioString.isEmpty()) {
            mostrarToast("Por favor, ingrese todos los campos");
            return;
        }

        // Obtener el precio como double
        double precio;
        try {
            precio = Double.parseDouble(precioString);
        } catch (NumberFormatException e) {
            mostrarToast("El precio debe ser un número válido");
            return;
        }

        // Obtener el valor de esPerecedero basándote en la selección de los RadioButtons
        boolean esPerecedero = false;
        if (rbtnPerecedero.isChecked()) {
            esPerecedero = true;
        } else if (rbtnNoPerecedero.isChecked()) {
            esPerecedero = false;
        } else {
            mostrarToast("Seleccione una opción");
            return;
        }

        // Validar el valor de precio, por ejemplo:
        if (precio <= 0) {
            mostrarToast("El precio debe ser mayor que cero");
            return;
        }

        ProductoDb productoDb = new ProductoDb(EditarProducto.this);
        Producto producto = new Producto(precio, codigo, nombre, marca, esPerecedero);

        // Verificar si el producto ya existe en la base de datos para decidir si actualizar o insertar
        Producto existingProducto = productoDb.getProductoPorCodigo(codigo);

        if (existingProducto == null) {
            // Si el producto no existe, mostrar mensaje de error
            mostrarToast("El producto con el código ingresado no existe");
            return;
        }

        // Actualizar el producto existente en la base de datos
        int filasActualizadas = productoDb.updateProducto(producto);

        if (filasActualizadas > 0) {
            mostrarToast("Producto actualizado con éxito");
        } else {
            mostrarToast("Error al actualizar el producto");
        }
    }

    private void eliminarProducto() {
        String codigo = txtCodigo.getText().toString().trim();

        if (!codigo.isEmpty()) {
            AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
            confirmar.setTitle("Confirmar");
            confirmar.setMessage("¿Estás seguro de que deseas eliminar este producto?");
            confirmar.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    ProductoDb productoDb = new ProductoDb(EditarProducto.this);
                    int filasEliminadas = productoDb.deleteProducto(codigo);

                    if (filasEliminadas > 0) {
                        mostrarToast("Producto eliminado correctamente");
                        limpiarCampos();
                    } else {
                        mostrarToast("No se encontró ningún producto con ese código");
                    }
                }
            });
            confirmar.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    // No hacer nada, simplemente cerrar el cuadro de diálogo
                }
            });
            confirmar.show();
        } else {
            mostrarToast("Por favor, ingrese el código del producto a eliminar");
        }
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Tienda");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                // No hacer nada
            }
        });
        confirmar.show();
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    private void limpiarCampos() {
        txtCodigo.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtPrecio.setText("");
        rbtnPerecedero.setChecked(false);
        rbtnNoPerecedero.setChecked(false);
    }

}
