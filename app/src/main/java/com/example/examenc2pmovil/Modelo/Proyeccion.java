package com.example.examenc2pmovil.Modelo;

import android.database.Cursor;

import com.example.examenc2.Producto;

import java.util.ArrayList;

public interface Proyeccion {
    Producto getProducto(int id);
    ArrayList<Producto> allProductos();
    Producto readProducto(Cursor cursor);
    // Agrega otros métodos de proyección necesarios
}
