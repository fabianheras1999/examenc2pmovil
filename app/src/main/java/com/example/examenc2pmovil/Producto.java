package com.example.examenc2pmovil;

import android.widget.RadioButton;
import java.io.Serializable;

public class Producto implements Serializable {
    private int id;
    private double precio;
    private String codigo;
    private String nombre;
    private String marca;
    private boolean perecedero;

    public Producto() {
    }

    public Producto(double precio, String codigo, String nombre, String marca, boolean perecedero) {
        this.precio = precio;
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca = marca;
        this.perecedero = perecedero;
    }

    // Getters y setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public boolean isPerecedero() {
        return perecedero;
    }

    public void setPerecedero(boolean perecedero) {
        this.perecedero = perecedero;
    }
}
